# Chatable

## Libraires & Tools
- [Nrwl Nx](https://nx.dev) : Scaffolding, build, lint, serve code

## Serve

### Angular app

  > ng serve chatable --browserTarget=chatable:build --port=3200 

or

  > npx ng serve chatable --browserTarget=chatable:build --port=3200 

  - Open: http://localhost:3200

### Node Server
  > ng serve server --buildTarget=server:build 

or 
  
  > npx ng serve server --buildTarget=server:build

  - Open: http://localhost:3333/api
