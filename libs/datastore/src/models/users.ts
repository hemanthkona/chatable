import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';

const Schema = mongoose.Schema;

const user = {
  username: String,
  password: String,
  _created_date: {
    type: Date,
    default: Date.now
  }
};

export const userSchema = new Schema(user);

// userSchema.methods.createUser = createUser;

export const User = mongoose.model('User', userSchema);

User.createUser = function(newUser, callback){
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};

User.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    if(err) throw err;
    callback(null, isMatch);
  });
}
