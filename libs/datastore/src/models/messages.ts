import * as mongoose from 'mongoose';
import { User } from './users';

const Schema = mongoose.Schema;

const message = {
  text: {
    type: String,
  },
  _user: {
    type: Schema.Types.ObjectId,
    ref: User,
    require: true
  },
  username: {
    type: String
  },
  _created_date: {
    type: Date,
    default: Date.now
  },
  _updated_date: {
    type: Date
  },
  liked: {
    type: Boolean,
    default: false
  },
  liked_users: [{
    type: Schema.Types.ObjectId,
    ref: User
  }]
}


export const messageSchema = new Schema(message);

export const Message = mongoose.model('Message', messageSchema);
