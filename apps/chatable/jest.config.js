module.exports = {
  name: 'chatable',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/chatable',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
