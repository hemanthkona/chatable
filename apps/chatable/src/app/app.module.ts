import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { AppLandingComponent } from './app.landing.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { APP_MATERIAL_MODULES } from './material-components';
import { ReactiveFormsModule } from '@angular/forms';

import { routes } from './app.routes';
import { LoginModule } from './login/login.module';
import { ChatroomModule } from './chatroom/chatroom.module';

@NgModule({
  declarations: [AppComponent, AppLandingComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,

    RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules  }),

    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }),

    ...APP_MATERIAL_MODULES,

    LoginModule,
    ChatroomModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
