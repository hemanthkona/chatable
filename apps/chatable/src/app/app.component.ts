import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { filter} from 'rxjs/operators';
import { ApiService, User } from './api.service';

@Component({
  selector: 'chatable-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Chatable';
  showLoginButton = true;
  showLogoutButton = false;

  user: User;

  constructor(private api: ApiService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private toast: ToastrService) {

  }

  getUsername() {
    if (ApiService && ApiService.loggedInUser && ApiService.loggedInUser.username)
      return ApiService.loggedInUser.username;
    else return '';
  }

  logout() {
    this.api.logout().subscribe(
      (data) => {
        this.api.clearLoginUser();
      },
      (err) => {
        this.api.clearLoginUser();
      }
    );
  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      ).subscribe((route: NavigationEnd) => {
        if (route.url.includes('login') || ApiService.isLoggedIn()) this.showLoginButton  = false;
        else this.showLoginButton = true;

        if (ApiService.loggedInUser) {
          this.showLogoutButton = true;

        } else {
          this.showLogoutButton = false;
        }
      });

    // redirect to Chatroom if loggedIn
    this.api.checkUser();
    this.api.initPusher();
  }
}
