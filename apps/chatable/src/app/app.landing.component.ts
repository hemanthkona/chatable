import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'chatable-root',
  template: `
    <h1> Welcome </h1>
  `,
  styles: [`
    :host {
      text-align: center;
    }
  `]
})
export class AppLandingComponent implements OnInit {
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.checkUser();
  }
}
