import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';

import * as Pusher from 'pusher-js';

import { ToastrService } from 'ngx-toastr';

const httpOptions = {
  // headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true //this is required so that Angular returns the Cookies received from the server. The server sends cookies in Set-Cookie header. Without this, Angular will ignore the Set-Cookie header
};

export interface User {
  _id: String;
  username: String;
  password?: String;
  _created_date?: String;
  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public static loggedInUser: User;

  BASE_URL = 'http://localhost:3333/api';
  USER_URL = '/user';
  LOGIN_URL = '/login';
  LOGOUT_URL = '/logout';
  REGISTER_URL = '/register';
  MESSAGES_URL = '/messages';

  pusher: any;
  channel: any;

  constructor(private http: HttpClient, private router: Router, private toast: ToastrService) { }

  public static isLoggedIn() {
    if (ApiService.loggedInUser && ApiService.loggedInUser._id) return true;
    else return false;
  }

  public initPusher() {
    this.pusher = new Pusher('4602fb9e3edf4c39e1d5', {
      cluster: 'us2',
      forceTLS: true
    });

    this.channel = this.pusher.subscribe('messages');
  }

  public checkUser() {
    if (window.localStorage) {
      console.log("Check User localstorage")
      const storedUser = localStorage.getItem('loggedInUser');
      if (storedUser) {
        ApiService.loggedInUser = JSON.parse(storedUser);
        this.router.navigate(['chatroom']);

      } else {
        this.router.navigate(['login']);
      }

    } else {
      this.getUser().subscribe(
        (data: User) => {
          ApiService.loggedInUser = data;

          this.router.navigate(['chatroom']);
        },
        (err) => {
          console.log(err);
          this.router.navigate(['login']);
        }
      );
    }
  }

  public setLoginUser(data) {
    if (window.localStorage) {
      console.log("Set User localstorage");
      ApiService.loggedInUser = data;
      localStorage.setItem('loggedInUser', JSON.stringify(ApiService.loggedInUser));

      this.toast.success('Success: User Login');
    }
  }

  public clearLoginUser() {
    if (window.localStorage) {
      console.log("CLear user localstorage");
      ApiService.loggedInUser = undefined;
      localStorage.removeItem('loggedInUser')

      this.toast.success('Success: User Logout');

      this.router.navigate(['login']);
    }
  }

  public register(body) {
    return this.http.post(this.BASE_URL + this.REGISTER_URL, body);
  }

  public login(body) {
    const url = this.BASE_URL + this.LOGIN_URL;
    return this.http.post(url, body);
  }

  public logout() {
    return this.http.get(this.BASE_URL + this.LOGOUT_URL);
  }

  public getUser() {
    return this.http.get(this.BASE_URL + this.USER_URL);
  }

  public getMessages() {
    return this.http.get(this.BASE_URL + this.MESSAGES_URL);
  }

  public createMesasge(body) {
    return this.http.post(this.BASE_URL + this.MESSAGES_URL, body);
  }

  public updateMesasge(id: String, body) {
    return this.http.put(this.BASE_URL + this.MESSAGES_URL + '/' + id + '/' + 'like', body);
  }

}
