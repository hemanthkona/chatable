import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { APP_MATERIAL_MODULES } from '../material-components';

import { ChatroomComponent } from './chatroom.component';
import { routes } from './chatroom.routes';

@NgModule({
  declarations: [ChatroomComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    FlexLayoutModule,

    ...APP_MATERIAL_MODULES
  ]
})
export class ChatroomModule { }
