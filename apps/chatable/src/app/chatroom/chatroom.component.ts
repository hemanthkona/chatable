import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

import { ApiService, User } from '../api.service';

@Component({
  selector: 'chatable-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit {

  message: FormGroup;

  messages: any;

  currentUser: User;

  constructor(private api: ApiService, private fb: FormBuilder, private toast: ToastrService) { }

  send() {
    if (this.message.invalid) return;

    const message = this.message.value;
    if (ApiService.loggedInUser && ApiService.loggedInUser._id) {
      message._user = ApiService.loggedInUser._id;
      message.username = ApiService.loggedInUser.username;
    }

    this.api.createMesasge(message).subscribe(
      (data) => {
        this.message.reset();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateMessage(action: String, message) {
    const id = message._id;
    const user = ApiService.loggedInUser._id;

    const updateMessage = {
      action: action,
      user: user
    };

    this.api.updateMesasge(id, updateMessage).subscribe(
      (data: any) => {


      },
      (err) => {
        this.toast.error("Error: Updating Message");
      }
    )
  }

  likeMessage(message) {
    this.updateMessage('like', message);
  }

  unlikeMessage(message) {
    this.updateMessage('unlike', message);
  }

  checkCurrentUser(message) {
    let result = false;
    if (message && message._user && ApiService.loggedInUser
      && ApiService.loggedInUser._id && message._user === ApiService.loggedInUser._id) {
      result = true;
    }

    return result;
  }

  initForm() {
    this.message = this.fb.group({
      text: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!ApiService.loggedInUser) this.api.checkUser();
    else {

      this.initForm();

      this.api.getMessages().subscribe(
        (data: []) => {
          data.forEach((message: any) => {
            if (message && message.liked_users && message.liked_users.indexOf(ApiService.loggedInUser._id) !== -1)  {
              message.liked = true;
            }
          });
          this.messages = data;
        },
        (err) => {
          console.log(err);
        }
      );

      this.api.channel.bind('newMessage', (data) => {
        this.messages = [data, ...this.messages];
        console.log("Pusher: New message")
        console.log(data);
      });

      this.api.channel.bind('updateMessage', (data) => {
        console.log("Pusher: Update Message")
        console.log(data);

        if (data && data.liked_users.indexOf(ApiService.loggedInUser._id) !== -1) {
          data.liked = true;
        }

        let itemIndex: string;
        for (const index in this.messages) {
          if (this.messages[index]._id === data._id) {
            itemIndex = index;
            break;
          }
        }

        this.messages[itemIndex] = data;
      });

    }
  }

}
