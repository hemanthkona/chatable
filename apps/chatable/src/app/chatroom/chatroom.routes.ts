import { Routes } from '@angular/router';

import { ChatroomComponent } from './chatroom.component';

import { IsLoggedInGuard } from '../is-logged-in.guard';

export const routes: Routes = [
  {
    path: 'chatroom',
    children: [{
      path: '',
      component: ChatroomComponent,
      // canActivate: [IsLoggedInGuard]
    }]
  }
];
