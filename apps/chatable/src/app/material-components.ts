import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatListModule,
  MatIconModule
} from '@angular/material';

export const APP_MATERIAL_MODULES = [
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatListModule,
  MatIconModule
];
