import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

import { ApiService, User } from '../api.service';

@Component({
  selector: 'chatable-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public title = 'Login';
  public isLoginView = true;
  public loginForm: FormGroup;
  public user: any;
  public error: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private fb: FormBuilder,
    private toast: ToastrService) {

  }

  submit(action) {
    // console.log(action);
    // console.log(this.loginForm.value);

    if (this.loginForm.invalid) return;

    const loginBody = this.loginForm.value;

    if (action === 'register') {
      this.api.register(loginBody).subscribe(
        (data: User) => {
          this.user = data;

          this.api.setLoginUser(data);

          this.router.navigate(['chatroom']);
        },
        (err) => {
          console.log(err);
          if (err.error && err.error.message) {
            this.error = err.message

            this.toast.error(err.error.message);

          } else {
            this.error = err;

            this.toast.error(JSON.stringify(err.error));
          }
        });

    } else if (action === 'login') {
      this.api.login(loginBody).subscribe(
        (data: User) => {
          this.user = data;

          this.api.setLoginUser(data);

          this.router.navigate(['chatroom']);
        },
        (err) => {
          console.log(err);
          this.error = "User not found";
          this.toast.error('User not found');
        });
    }
  }

  getUser() {
    this.api.getUser().subscribe(
      (data) => {
        this.user = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getMessages() {
    this.api.getMessages().subscribe(
      (data) => {
        this.user = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  logout() {
    this.api.logout().subscribe(
      (data) => {
        this.user = undefined;
        this.api.clearLoginUser();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  initForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
    // redirect to Chatroom if loggedIn
    this.api.checkUser();

    // if (this.router.url.includes('login')) {
    //   this.title = "Login";
    //   this.isLoginView = true;

    // } else if (this.router.url.includes('register')) {
    //   this.title = "Register";
    //   this.isLoginView = false;
    // }

    this.initForm();
  }

}
