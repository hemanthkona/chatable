import { Routes } from '@angular/router';

import { LoginComponent } from './login.component';

export const routes: Routes = [
  {
    path: 'login',
    children: [{
      path: '',
      component: LoginComponent
    }]
  },
  {
    path: 'register',
    children: [{
      path: '',
      component: LoginComponent
    }]
  }
];
