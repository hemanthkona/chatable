import { Routes } from '@angular/router';
import { AppLandingComponent } from './app.landing.component';

export const routes: Routes = [
  {
    path: '',
    component: AppLandingComponent
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(mod => mod.LoginModule)
  },
  {
    path: 'chatroom',
    loadChildren: () => import('./chatroom/chatroom.module').then(mod => mod.ChatroomModule)
  }
];
