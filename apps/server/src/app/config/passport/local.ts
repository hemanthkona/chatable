import mongoose from 'mongoose';
import { Strategy as LocalStrategy} from 'passport-local';

import { User } from '@chatable/datastore';

/**
 * Expose
 */

export const strategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password'
  },
  function(username, password, done) {
    const options = {
      criteria: { username: username }
    };

    User.findOne(options.criteria, function(err, user) {
      if (err) done(err);

      if (!user) {
        return done(null, false, { message: 'Unknown user' });
      }

      User.comparePassword(password, user.password, (err, isMatch) => {
        if(err) done(err);

        if(isMatch){
          return done(null, user);

        } else {
          return done(null, false, {message: 'Invalid password'});
        }
      })
    });
  }
);
