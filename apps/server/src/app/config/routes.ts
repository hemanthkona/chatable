import { Message, User } from '@chatable/datastore';

export default function(router, pusher, passport) {

  router.get('/', (req, res) => {
    res.status(200).send({'api':'Hello'});
  });

  router.get('/logout', function(req, res){
    if (req.user) {
      req.logout();
      res.send({"message": "Successfully logged out"});

    } else {
      res.send({"message": "User not found"});
    }
  });

  router.get('/user', (req, res) => {
    const testUser = {
      _id: "5e374c2331cbe34ae25b9899",
      username: "test",
      _test: true
    };

    if (req.user) {
      res.send(req.user);

    } else {
      // res.send(testUser);
      res.status(401).send({
        message: 'User not logged in'
      });
    }
  });

  router.post('/login', passport.authenticate('local'), (req, res) => {
    res.send(req.user);
  });

  router.post('/register', (req, res, next) => {
    const body = req.body;

    if (body.username.length <= 0 || body.password.length <= 0) {
      res.status(400).send({
        'message': "Username or password is invalid"
      });

    } else {
      const username = body.username;
      const password = body.password;

      User.findOne({username: username}, (err, user) => {
        if (err) next(err);
        if (user) {
          res.status(400).send({
            "message": "Username is taken, please use a different username"
          })
        } else {
          const newUser = new User({
            username: username,
            password: password
          });

          User.createUser(newUser, (error, user) => {
            if (err) next(error);

            // req.login(user, function(err) {
            //   if (err) { return next(err); }
            //   res.send(user);
            // });

            res.send(user);
          })
        }
      })
    }



    // User.save((err, user) => {
    //   if (err) next(err);
    //   res.send(user);
    // });
  });


  router.get('/messages', (req, res, next) => {
    Message.find({}, null, {sort: {_created_date: -1}})
      // .popuplate('user')
      .exec((err, messages) => {
        if (err) next(err);

        res.send(messages);
      });
  })

  router.post('/messages', (req, res, next) => {
    const newMessage = new Message(req.body);

    newMessage.save((err, message) => {
      if (err) next(err);

      pusher.trigger('messages', 'newMessage', message);

      res.send(message);
    })

  });

  router.put('/messages/:id/like', (req, res, next) => {
    const id = req.params.id;
    const user = req.body.user;

    if (req.body.action === 'like') {
      Message.findOneAndUpdate(
        {_id: id},
        { '$addToSet': { 'liked_users': user } },
        { new: true },
        (err, message) => {
          if (err) next(err);

          pusher.trigger('messages', 'updateMessage', message);

          res.send(message);
        });

    } else if (req.body.action === 'unlike') {
      Message.findOneAndUpdate(
        {_id: id},
        { '$pull': { 'liked_users': user } },
        { new: true },
        (err, message) => {
          if (err) next(err);

          pusher.trigger('messages', 'updateMessage', message);

          res.send(message);
        });
    }

  });

}
