import mongoose from 'mongoose';
import * as local from './passport/local';

const User = mongoose.model('User');

/**
 * Expose
 */

export default function(passport) {
  // serialize and deserialize sessions
  passport.serializeUser((user, done) => done(null, user.id));
  passport.deserializeUser((id, done) => User.findOne({ _id: id }, done));

  // use these strategies
  passport.use(local.strategy);
};
