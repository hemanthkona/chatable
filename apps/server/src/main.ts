/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import * as session from 'express-session';
import * as mongoose from 'mongoose';
import * as connectMongo from 'connect-mongo';
import * as flash from 'connect-flash';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as passport from 'passport';

import { User, Message } from '@chatable/datastore';

import passportInit from './app/config/passport';
import pusherInit from './app/config/pusher';
import routes from './app/config/routes';

const port = process.env.PORT || 3333;
const mongoUri = "mongodb+srv://test:test@el-chatable-jogcn.mongodb.net/mintbean_chatable?retryWrites=true&w=majority";

const MongoStore = connectMongo(session);

const app = express();
const router = express.Router();
const connection = connect();

const pusher = pusherInit();

// init passport
passportInit(passport);

// app.use(helmet());
// app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });
// cookieParser should be above session
app.use(cookieParser());
app.use(
  session({
    secret: 'secret',
    proxy: true,
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({
      mongooseConnection: connection
    })
  })
);

// use passport session
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use('/api', router);
app.use(function (err, req, res, next) {
  res.status(500).send(err);
})

connection.on('error', () => {
  console.log("Connection error");
});

connection.on('disconnected', () => {
  console.log("Connection disconnected");
  // connect();
});

connection.once('open', () => {
  console.log("Connection open");
  // console.log(connection.db.databaseName);

  // console.log(connection.db.listCollections().toArray((err, names) => {
  //   console.log(names);
  // }));

  listen();

  routes(router, pusher, passport);

})

function listen() {
  app.listen(port);
  console.log('Express app started on port ' + port);
}

function connect() {
  mongoose.set('useFindAndModify', false);
  // Connect to mongodb
  mongoose.connect(mongoUri, {
    keepAlive: 1,
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).catch((e) => {
    console.log("Connection error", e);
  });

  return mongoose.connection;
}
